package com.boons.mvvm.models.mahasiswa

data class Result(
    val id_mhs: String,
    val nama: String,
    val nim: String,
    val semester: String
)