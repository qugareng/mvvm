package com.boons.mvvm.models.mahasiswa

data class ResultData(
    val results: List<Result>
)