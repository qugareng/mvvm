package com.boons.mvvm.services

import com.boons.mvvm.models.mahasiswa.ResultData
import io.reactivex.Single
import retrofit2.http.*

interface Api {

    @GET("/mahasiswa.php")
    fun doSearch(
        @Query("stat") q: String?
    ): Single<ResultData>

    @GET("/mahasiswa.php")
    fun doInsert(
            @Query("stat") stat: String?,
            @Query("nim")  nim: String?,
            @Query("nama")  nama: String?,
            @Query("semester")  semester: String?
    ): Single<ResultData>
    

    @GET("/mahasiswa.php")
    fun doUpdate(
            @Query("stat") stat: String?,
            @Query("id") id: String?,
            @Query("nim")  nim: String?,
            @Query("nama")  nama: String?,
            @Query("semester")  semester: String?
    ): Single<ResultData>

    @GET("/mahasiswa.php")
    fun doDelete(
            @Query("stat") stat: String?,
            @Query("id") id: String?
    ): Single<ResultData>

}