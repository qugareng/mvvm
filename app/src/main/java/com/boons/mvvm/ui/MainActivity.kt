package com.boons.mvvm.ui

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.boons.mvvm.R
import com.boons.mvvm.databinding.ActivityMainBinding
import com.boons.mvvm.extentions.gone
import com.boons.mvvm.extentions.visible
import com.boons.mvvm.models.mahasiswa.Result
import com.boons.mvvm.adapter.MainAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog.*
import kotlinx.android.synthetic.main.dialog.view.*

class  MainActivity : AppCompatActivity(), MainView.View {

    private val TAG = this::class.java.simpleName
    private lateinit var binding: ActivityMainBinding
    private lateinit var presenter: MainPresenter
    private val list: MutableList<Result> = mutableListOf()
    private lateinit var adapterdata: MainAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        btn_add.setOnClickListener {
            showAlertDialog("create", "", "", "", "")
        }

        list_data()
    }

     fun list_data() {
        presenter = MainPresenter()
        presenter.setupView(this)

        val mLayoutManager = LinearLayoutManager(this)
        adapterdata = MainAdapter(list,this){
            Toast.makeText(this@MainActivity, it.nama, Toast.LENGTH_SHORT).show()
        }

//        binding.rvAnime.layoutManager= mLayoutManager

        binding.rvAnime.apply {
            this.layoutManager = mLayoutManager
            this.setHasFixedSize(true)
            this.adapter = adapterdata
        }


        presenter.search("list")
    }

    override fun setList(resultlist: List<Result>) {
        binding.apply {
            rvAnime.visible()
        }
        list.clear()
        list.addAll(resultlist)

        adapterdata.notifyDataSetChanged()
    }

    override fun showProgress() {
        binding.apply {
            this.rvAnime.gone()
            this.btnAdd.gone();
            this.viewLoading.visible()
        }
    }

    override fun hideProgress() {
        binding.apply {
            this.rvAnime.visible()
            this.btnAdd.visible();
            this.viewLoading.gone()
        }
    }

    fun showAlertDialog(vald : String?, val_id : String?, val_nim : String?, val_nama : String?, val_semester : String?) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)

        val nim = mDialogView.et_nim
        val nama = mDialogView.et_nama
        val semester = mDialogView.et_semester

        if (vald.equals("update")){
            mDialogView.title.setText("UPDATE")
            nim.setText(val_nim)
            nama.setText(val_nama)
            semester.setText(val_semester)
        }else{
            mDialogView.title.setText("CREATE")
        }

        val mAlertDialog = mBuilder.show()
        mDialogView.btn_insert.setOnClickListener {
            val get_nim       = nim.text.toString()
            val get_nama      = nama.text.toString()
            val get_semester  = semester.text.toString()
//            Toast.makeText(this@MainActivity, "show data :  $val_id  |  $get_nim  |  $get_nama  |  $get_semester", Toast.LENGTH_SHORT).show()
            presenter = MainPresenter()
            presenter.setupView(this)
            if (vald.equals("update")){
                presenter.update(vald, val_id, get_nim, get_nama, get_semester)
            }else{
                presenter.insert(vald, get_nim, get_nama, get_semester)
            }
            mAlertDialog.dismiss()
        }
        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    fun showDelete(vald : String?, id : String?, nim : String?){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("DELETE")
        builder.setMessage("Apakah anda ingin menghapus mahasiswa nim : $nim ?")

        builder.setPositiveButton(R.string.TextOk) { dialog, which ->
            presenter = MainPresenter()
            presenter.setupView(this)
            presenter.delete(vald, id)
        }

        builder.setNegativeButton(R.string.TextCancel) { dialog, which ->
        }
        builder.show()
    }





}