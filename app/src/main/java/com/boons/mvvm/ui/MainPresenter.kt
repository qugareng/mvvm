package com.boons.mvvm.ui

import android.annotation.SuppressLint
import android.util.Log.d
import com.boons.mvvm.services.Api
import com.boons.mvvm.services.getApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.boons.mvvm.ui.MainActivity;
import retrofit2.Call


class MainPresenter : MainView.logic{
    private val TAG = this::class.java.simpleName
    private lateinit var view: MainView.View
    private var api: Api = getApi().create(Api::class.java)

    override fun setupView(view: MainView.View) {
        this.view = view
    }

    @SuppressLint("CheckResult")
    override fun search(q: String?) {
        view.showProgress()
        api.doSearch(q)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        {
                            view.hideProgress()
                            view.setList(it.results)
                            d("hanyatest", it.results.toString())
                        },

                        { d(TAG, "error: ${it.localizedMessage}") }
                )
    }

    @SuppressLint("CheckResult")
    fun insert(stat: String?, nim: String?, nama: String?, semester: String?) {
        api.doInsert(stat, nim, nama, semester)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        {
                            search("list")
                        },

                        { d(TAG, "error: ${it.localizedMessage}") }
                )
    }

    @SuppressLint("CheckResult")
    fun update(stat: String?, id: String?, nim: String?, nama: String?, semester: String?) {
        api.doUpdate(stat, id, nim, nama, semester)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        {
                            search("list")
                        },

                        { d(TAG, "error: ${it.localizedMessage}") }
                )
    }

    @SuppressLint("CheckResult")
    fun delete(stat: String?, id: String?) {
        api.doDelete(stat, id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        {
                            search("list")
                        },

                        { d(TAG, "error: ${it.localizedMessage}") }
                )
    }

}