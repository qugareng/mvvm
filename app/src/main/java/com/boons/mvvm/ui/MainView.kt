package com.boons.mvvm.ui

import com.boons.mvvm.extentions.BaseView
import com.boons.mvvm.models.mahasiswa.Result

interface MainView {
    interface View: BaseView.progressView {
        fun setList(list: List<Result>)
    }

    interface logic{
        fun setupView(view: View)
        fun search(q: String?)

    }

}