package com.boons.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.boons.mvvm.R
import com.boons.mvvm.databinding.ItemListBinding
import com.boons.mvvm.models.mahasiswa.Result
import com.boons.mvvm.ui.MainActivity;


class MainAdapter(

        private val item: List<Result>,
        private val mContext: Context,
        private val listener: (Result) -> Unit
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = ItemListBinding.inflate(
            LayoutInflater.from(mContext),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            with(item[position]){
                binding.tvNim.text = nim
                binding.tvNama.text = nama

                binding.btnEdit.setOnClickListener {
                    val popupMenu: PopupMenu = PopupMenu(mContext,binding.btnEdit)
                    popupMenu.menuInflater.inflate(R.menu.menu_option,popupMenu.menu)
                    popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                        when(item.itemId) {
                            R.id.action_edit ->
                                (mContext as MainActivity).showAlertDialog("update", id_mhs, nim, nama, semester)
                            R.id.action_delete ->
                                (mContext as MainActivity).showDelete("delete", id_mhs, nim)
                        }
                        true
                    })
                    popupMenu.show()

                }
//                Glide.with(mContext).load(image_url).into(binding.imgBeaner)
            }
        }
    }

    override fun getItemCount(): Int {
        return item.size
    }

    class ViewHolder(val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root)
}