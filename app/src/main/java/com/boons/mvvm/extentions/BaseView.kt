package com.boons.mvvm.extentions


interface BaseView {
    interface progressView{
        fun showProgress()
        fun hideProgress()
    }
}